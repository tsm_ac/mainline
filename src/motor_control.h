#ifndef motor_control_f
#define motor_control_f

extern float acc_max;
extern float acc_min;

extern float acc_left, acc_right;
extern float steer;


void stop();
void update_servo_command();
void calculate_motors_speed();
void calculate_motors_speed_speed_zone(int nr_linii);
#endif