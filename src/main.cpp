
#define blaa
#ifdef blaa
#include "TFC.h"
#include "TSISensor.h"
#include "image_processing.h"
#include "motor_control.h"
#include "tsm_ac.h"
#include "distance_monitoring.h"

#define NUM_TFC_TICKERS 4

#define TIMER_CHECK_WHEELS 0.020


#define LED_OFF            PTB->PSOR |= 1 << 18;
#define LED_ON             PTB->PCOR |= 1 << 18;
DigitalOut rled(LED1);
DigitalOut gled(LED2);
DigitalOut bled(LED3);

int obstacle_detected_flag;
TSISensor tsi_button;

InterruptIn stop_sensors_interrupt(PTA16);
Timeout set_straight_after_obstacle;

Serial PC(USBTX,USBRX);
RawSerial Xbee(PTE22, PTE23);

int read_region_from_touch()
{
    int state = 0;
    float x = 0.0;
    while(x < 0.001){
        x= tsi_button.readPercentage();
        if(x > 0.001){
            if (x < 0.3)
            {
                state = 1;
                rled = 0;
                gled= 1;
                bled = 1;
            }
            else if(x < 0.7){
                 state = 2;
                 rled = 1;
                 gled= 0;
                 bled = 1;
            }
            else
            {
                state = 3;
                rled = 1;
                gled= 1;
                bled = 0;    
            }
        }
    }
    wait_ms(100);
    PC.printf("State = %d \r\n", state);
    return state;
}

void read_speed_from_touch(float max_r1, float min_r1, float max_r2, float min_r2, float max_r3, float min_r3)
{    
    int state;
    state = read_region_from_touch();
    if(state == 1) {
        
        acc_max = max_r1;
        acc_min = min_r2;
    }
    else if(state == 2) {      
        acc_max = max_r2;
        acc_min = min_r2;
    }
    else {   
        acc_max = max_r3;
        acc_min = min_r3;
    }       
}

void secventa_test()
{
    DigitalIn Buton(PTC0);

    TFC_SetServo(0, 0);
    wait(1);
    if(Buton.read()) {
        //LED_ON;

    }

    TFC_SetServo(0, -1);
    wait(1);
    TFC_SetServo(0, 1);
    wait(1);
    TFC_SetServo(0, 0);
    wait(1);

}

float Kp = 0.0278; // 0.1
float Kd = 5;
float Ki = 0.0048;
float epsilon = 0.01;
float MAX = 1;
float MIN = -1;
float dt = 0.3;

float pre_error= 0;
float pid_error;
float integral = 0;
#define integral_max_val 20

float PIDcal(float setpoint,float actual_position)
{
    
    float derivative;
    float output;

    //CaculateP,I,D
    pid_error = setpoint -actual_position;
    integral = integral + dt * pid_error;
    if (integral > integral_max_val)
    {
        integral = integral_max_val;
    }
    if (integral < -integral_max_val)
    {
        integral = -integral_max_val;    
    }
    derivative= (float((pid_error -pre_error)))/40;
    
    output=Kp*pid_error + Kd*derivative + Ki*integral;
    //Saturation Filter
    if(output> MAX)
    {
        output= MAX;
    }
    else if(output< MIN)
    {
    output= MIN;
    }

    //Update error
    pre_error= pid_error;
    return output;
}


Ticker time_up;

// this should handle the stop of the car when it finishes the track
int flag_stop_car= 0;
float z;

void stop_function()
{
    flag_stop_car ++;
    if (flag_stop_car >= 2)
    {
        LED_ON;
    }
}


void any_configurations()
{
    TFC_Init();
 
    PORTD->PCR[6] = PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK;
    PORTD->PCR[7] = PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK;

    PTD->PDDR |= (1<<6);
    PTD->PSOR = (1<<6);

    PTD->PDDR |= (1<<7);
    PTD->PSOR = (1<<7);

    PORTB->PCR[18] = PORT_PCR_MUX(1);// | PORT_PCR_DSE_MASK;
    PTB->PDDR |= 1 << 18;
    LED_OFF;   
    
    initialize_distance_monitorisation();
}

void set_straight_after_obst()
{
    MEAN_OF_TRACK = 58;
    obstacle_detected_flag = 0;
}

void check_if_obstacle_occurs(int *x_new)
{
    /*         Daca distanta pana la obstacol e mai mica decat  ... (?)      */
    /*               incep sa merg pe partea opusa pentru un timp            */
    if(obstacle_detected_flag == 0)
    {
        if(calculated_distance_left < 50)
        {
            obstacle_detected_flag =1;
            MEAN_OF_TRACK = 80;
            set_straight_after_obstacle.attach(&set_straight_after_obst, 2);
            if(*x_new > -10){
                *x_new = -20;
            }
        }
        else if (calculated_distance_right < 50)
        {
            obstacle_detected_flag = 1;
            MEAN_OF_TRACK = 40;
            set_straight_after_obstacle.attach(&set_straight_after_obst, 2);
            if(*x_new < 10)
            {
                *x_new = 20;
            }
        }
    }
    /***************************************/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/************************************************DUTY FOR EACH TRACK ***********************************************************/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////*********************OBSTACLE AVOIDANCE TRACK********************///////////////////////////////////////
void track_obstacle_set_up()
{
    
}

void track_obstacle_main()
{
    PC.printf("Obstacle Avoidance Main \r\n");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////***********************SPEED ZONE TRACK*************************//////////////////////////////////////
void track_speed_zone_set_up()
{
    
}

void track_speed_zone_main()
{
    PC.printf("Speed Zone Main \r\n");
    
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////*************************FFFFINAAAALLLLLL TRACK*****************///////////////////////////////////////
void track_final_set_up()
{
    
}    

void track_final_duty()
{
    PC.printf("Final Track Main \r\n");
    int x_new;
    
    for(;;) {        
        if(TFC_LineScanImageReady>0) {
            TFC_LineScanImageReady=0;           
                 
            x_new = find_middle_error();
                                   
            steer = PIDcal(0, x_new);    // stanga = 1 // dreapta = -1 (desi la apelul functiei se ia negat)

            calculate_motors_speed();
            if(flag_stop_car >= 3)
            {
                TFC_SetMotorPWM(0, 0);
            }
            else
            {
                TFC_SetMotorPWM(-acc_left, -acc_right);
            }                 
        }
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////******************88888888888 TRACK**********************///////////////////////////////////////////////

void track_8_set_up()
{
    read_speed_from_touch(0.4, 0.33, 0.35, 0.30, 0.35, 0.25);
}

void track_8_main()
{
    PC.printf("8 Track Main \r\n");
    track_8_set_up();
    track_final_duty();   
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////*********TRACK SELECTION***********/////////////////////////////////////////////////////

void track_selection()
{
    int state = 2;
    // final or optional track
    while(state == 2) {
        state = read_region_from_touch();
    }
    if (state == 1) {
        // final track
         rled = 1;
         gled = 0;
         bled = 0;  
         track_final_duty();
    }
    else {
        // option track
        state = read_region_from_touch();
        rled = 0;
        gled = 0;
        bled = 1;
        if (state == 1) {
            track_8_main();
        } 
        else if (state == 2){
            track_obstacle_main();
        }
        else {
            track_speed_zone_main();
        }  
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main()
{
    PC.baud(115200);
    Xbee.baud(115200);
    stop_sensors_interrupt.mode(PullUp);
    stop_sensors_interrupt.fall(&stop_function);
    
    any_configurations();
    
    read_touch_for_camera_calibration();

    secventa_test();

    time_up.attach(&update_servo_command, TIMER_CHECK_WHEELS);
    
    track_selection();

}

#endif

#define blaa
#ifdef blaa
#include "TFC.h"
#include "TSISensor.h"
#include "image_processing.h"
#include "motor_control.h"
#include "tsm_ac.h"
#include "distance_monitoring.h"

#define NUM_TFC_TICKERS 4

#define TIMER_CHECK_WHEELS 0.020


#define LED_OFF            PTB->PSOR |= 1 << 18;
#define LED_ON             PTB->PCOR |= 1 << 18;
DigitalOut rled(LED1);
DigitalOut gled(LED2);
DigitalOut bled(LED3);

int obstacle_detected_flag;
TSISensor tsi_button;

InterruptIn stop_sensors_interrupt(PTA16);
Timeout set_straight_after_obstacle;

Serial PC(USBTX,USBRX);
RawSerial Xbee(PTE22, PTE23);


void secventa_test()
{
    DigitalIn Buton(PTC0);

    TFC_SetServo(0, 0);
    wait(1);
    if(Buton.read()) {
        //LED_ON;

    }

    TFC_SetServo(0, -1);
    wait(1);
    TFC_SetServo(0, 1);
    wait(1);
    TFC_SetServo(0, 0);
    wait(1);

}

float Kp = 0.0275; // 0.1
float Kd = 5;
float Ki = 0.0045;
float epsilon = 0.01;
float MAX = 1;
float MIN = -1;
float dt = 0.3;

float pre_error= 0;
float pid_error;
float integral = 0;
#define integral_max_val 20

float PIDcal(float setpoint,float actual_position)
{
    
    float derivative;
    float output;

    //CaculateP,I,D
    pid_error = setpoint -actual_position;
    integral = integral + dt * pid_error;
    if (integral > integral_max_val)
    {
        integral = integral_max_val;
    }
    if (integral < -integral_max_val)
    {
        integral = -integral_max_val;    
    }
    derivative= (float((pid_error -pre_error)))/40;
    
    output=Kp*pid_error + Kd*derivative + Ki*integral;
    //Saturation Filter
    if(output> MAX)
    {
        output= MAX;
    }
    else if(output< MIN)
    {
    output= MIN;
    }

    //Update error
    pre_error= pid_error;
    return output;
}


Ticker time_up;

// this should handle the stop of the car when it finishes the track
int flag_stop_car= 0;
float z;

void stop_function()
{
    flag_stop_car ++;
    if (flag_stop_car >= 2)
    {
        LED_ON;
    }
}


void read_speed_from_touch()
{    
    float x = 0.0;
    while(x < 0.001){
    x= tsi_button.readPercentage();
    if(x > 0.001){
    if (x < 0.3)
    {
        rled = 0;
        gled= 1;
        bled = 1;
        acc_max = 0.3;
    }
    else if(x<0.7)
    {
        rled = 1;
        gled= 0;
        bled = 1;
        acc_max = 0.35;
    }
    else
    {
        rled = 1;
        gled= 1;
        bled = 0;
        acc_max = 0.4;
    }
    }
    //PC.printf("%f \r\n", x);
    wait_ms(20);
    }    
}


void any_configurations()
{
    TFC_Init();
 
    PORTD->PCR[6] = PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK;
    PORTD->PCR[7] = PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK;

    PTD->PDDR |= (1<<6);
    PTD->PSOR = (1<<6);

    PTD->PDDR |= (1<<7);
    PTD->PSOR = (1<<7);

    PORTB->PCR[18] = PORT_PCR_MUX(1);// | PORT_PCR_DSE_MASK;
    PTB->PDDR |= 1 << 18;
    LED_OFF;   
    
    initialize_distance_monitorisation();
}

void set_straight_after_obst()
{
    MEAN_OF_TRACK = 58;
    obstacle_detected_flag = 0;
}

void check_if_obstacle_occurs(int *x_new)
{
    /*         Daca distanta pana la obstacol e mai mica decat  ... (?)      */
    /*               incep sa merg pe partea opusa pentru un timp            */
    if(obstacle_detected_flag == 0)
    {
        if(calculated_distance_left < 50)
        {
            obstacle_detected_flag =1;
            MEAN_OF_TRACK = 80;
            set_straight_after_obstacle.attach(&set_straight_after_obst, 2);
            if(*x_new > -10){
                *x_new = -20;
            }
        }
        else if (calculated_distance_right < 50)
        {
            obstacle_detected_flag = 1;
            MEAN_OF_TRACK = 40;
            set_straight_after_obstacle.attach(&set_straight_after_obst, 2);
            if(*x_new < 10)
            {
                *x_new = 20;
            }
        }
    }
    /***************************************/
}

int main()
{
    PC.baud(115200);
    Xbee.baud(115200);
    stop_sensors_interrupt.mode(PullUp);
    stop_sensors_interrupt.fall(&stop_function);
    
    any_configurations();
    
    int x_new;
    
    read_touch_for_camera_calibration();
    read_speed_from_touch();

    secventa_test();

    time_up.attach(&update_servo_command, TIMER_CHECK_WHEELS);
    
    for(;;) {        
        if(TFC_LineScanImageReady>0) {
            TFC_LineScanImageReady=0;           
                 
            x_new = find_middle_error();
                                   
            steer = PIDcal(0,x_new);    // stanga = 1 // dreapta = -1 (desi la apelul functiei se ia negat)

            calculate_motors_speed();
            if(flag_stop_car >= 3)
            {
                TFC_SetMotorPWM(0, 0);
            }
            else
            {
                TFC_SetMotorPWM(-acc_left, -acc_right);
            }  
            //PC.printf("Distance_st = %d\tDistance_dr = %d\r\n",calculated_distance_left, calculated_distance_right);                     
        }
    }

}

#endif
