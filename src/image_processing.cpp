
#include "TFC.h"
#include "image_processing.h"
#include "tsm_ac.h"

/* defines */
//intensitatea luminii mare, creste diff_tresh: 30
#define DIFF_THRESH     80
#define FRAME_DROP      100
#define MEDIE_THRESH    20//de completat

/* variables */
int MEAN_OF_TRACK = 58;
int pixels_save[4]={0};
int x_old = 0;
int middle_pixel_intensity = 0;
int max_pixel_calibration = 0;

float medie_old = 0;

uint16_t camera_calib_params[128];

uint16_t current_camera_frame[128];
/* functions */

float medie_new = 0;
void gaseste_liniile_prin_parcurgereVector()
{
    
    
    medie_new = medie_pixeli();
    
    
    //int max_pixel = 0;
    int i = 0;

    black_pixel_counter = 0;
    //PC.printf("Diff: %f\r\n", abs(medie_old-medie_new));
    if(medie_old != 0 && abs(medie_old-medie_new) > MEDIE_THRESH)
    {       
        
        for (i = 35; i < 90; i+=3)
        {
            if ( current_camera_frame[i-1] * DIFF_THRESH > current_camera_frame[i+1] * 100 )
            {
                //i += 3;
                
                black_pixel_counter++; //aici ar trebui sa incrementeze doar cand determina diferentele intre muchii
                
            }
        }
        
        //logica determinare numar de linii -> in functie de numarul de pixeli negri gasiti ?!
        
        
        // la final, resetam counter-ul pentru pixeli negri
        
    }
    else {
        medie_old = medie_new;
    }
    nr_linii = black_pixel_counter;
    if (nr_linii != 0) {
            PC.printf("\t\t\t\tLinii aparute: %d\r\n", nr_linii);    
    }
    
}

float medie_pixeli()
{
    int i;
    float sum = 0;
    float medie;
    for(i = 0; i < 128; i++)
    {
        //PC.printf("%d ", TFC_LineScanImage0[i]);
        sum += TFC_LineScanImage0[i];
    }
    //PC.printf("\r\n");
    medie = (float)sum / 128;
        
    return medie;
}



void adapt_exposure_time()
{
     // 3.2ms <-> 32ms to take an img
    if(middle_pixel_intensity >4000 && light_intensity_adapter > 2000)
    {
        light_intensity_adapter -= 200;
    }
    else if (middle_pixel_intensity < 3000 && light_intensity_adapter < 20000)
    {
        light_intensity_adapter += 200;
    }    

}


void initial_exposure_calibration()
{
    TFC_LineScanImageReady = 0;
    
    while(TFC_LineScanImageReady == 0);
    TFC_LineScanImageReady= 0;

    middle_pixel_intensity = TFC_LineScanImage0[MEAN_OF_TRACK];

    while (middle_pixel_intensity < 3000 || middle_pixel_intensity > 4000)
    {
        adapt_exposure_time();

        while(TFC_LineScanImageReady == 0);
        TFC_LineScanImageReady= 0;

        middle_pixel_intensity = TFC_LineScanImage0[MEAN_OF_TRACK];
    }
}

void read_touch_for_camera_calibration()
{
    float x = 0.0;
    rled = 0;
    gled= 0;
    bled = 0;
    while(x < 0.001){
    x = tsi_button.readPercentage();
    wait_ms(20);
    }
    
    initial_exposure_calibration();

    TFC_LineScanImageReady = 0;
    
    while(TFC_LineScanImageReady == 0);
    TFC_LineScanImageReady= 0;


    /*      adun 5 sampleuri         */
    for(int i=0;i<128;i++)
    {
        current_camera_frame[i] = TFC_LineScanImage0[i];
    }
    for(int j=0;j<4;j++)
    {
        while(TFC_LineScanImageReady == 0);
        TFC_LineScanImageReady = 0;
        for(int i=0;i<128;i++)
        {
            current_camera_frame[i] += TFC_LineScanImage0[i];
        }
    }
    for(int i=0;i<128;i++)
    {
        camera_calib_params[i] = (current_camera_frame[i] / 5)+1;
    }
    /*******************************/

    for(int i = 0; i < 128; i++)
    {
        if (max_pixel_calibration < camera_calib_params[i])
        {
            max_pixel_calibration = camera_calib_params[i];
        }                    
        
    }
    
    for(int i=0;i<128;i++)
    {
        camera_calib_params[i] = max_pixel_calibration - camera_calib_params[i];
    }

    PC.printf("<");
    for (int i=0;i<127;i++)
    {
        PC.printf("%d,",camera_calib_params[i]);
    }
    
    PC.printf("%d>\r\n",camera_calib_params[127]);
    wait_ms(1000);
    rled = 0;
    gled= 1;
    bled = 0;
}

void print_camera_frame()
{
    PC.printf("<");
    for (int i=0;i<127;i++)
    {
        PC.printf("%d,",current_camera_frame[i]);
    }
    PC.printf("%d>\r\n",current_camera_frame[127]);
}

int find_middle_error()
{
    int i;
    int left, right;
    int x_new;
    int index_of_middle_pixel;
    int max_pixel=0;
    
    for(int i = 0; i < 128; i++)
    {
        current_camera_frame[i] = (TFC_LineScanImage0[i]);
        if (max_pixel < current_camera_frame[i])
        {
            max_pixel = current_camera_frame[i];
        }                    
        
    }
    middle_pixel_intensity = max_pixel;
    adapt_exposure_time();
    /* astea as vrea sa vad cum arata pe grafic */
    for(i=0;i<128;i++)
    {
        // adaug proportia care lipseste pixelului (pe baza calibrarii)
        current_camera_frame[i] = current_camera_frame[i] + (camera_calib_params[i]* max_pixel)/max_pixel_calibration;
    }
    //print_camera_frame();

    right = 120;
    for(i = x_old + MEAN_OF_TRACK; i < 127; i++) {
        
        if ( current_camera_frame[i-1] * DIFF_THRESH > current_camera_frame[i+1] * 100 ) {
            
                right = i;
                break;
        }
    }
            
    left = 8;
    for(i = x_old +MEAN_OF_TRACK; i > 1; i--) {
        if (  current_camera_frame[i+1] * DIFF_THRESH > current_camera_frame[i-1] * 100) {
          
                left = i;
                break;
        }
    }      

    x_new = ((left + right) / 2 ) - MEAN_OF_TRACK; 

    x_old = x_new;  
    return x_new;
}