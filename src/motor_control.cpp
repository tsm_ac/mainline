#include "motor_control.h"
#include "TFC.h"

float acc_max = 0.35;
float acc_min = 0.3;
float acc_left= acc_min, acc_right = acc_min;
int time_to_acc = 10;
float steer =0 ;



void calculate_motors_speed_zone(int nr_linii)
{
     
    uint16_t flag_MotorLow = 0;
    uint16_t flag_MotorHigh = 0;
    uint16_t flag_SpeedZone = 0;
    
    //LED_ON;
    rled = 1;

    if(nr_linii > 2)
    {
        flag_SpeedZone = 1;   
        if(max_linii < nr_linii)
        {
            max_linii = nr_linii;    
        }
    }
    else
    {
        flag_SpeedZone = 0;
        max_linii = 0;    
    }
    
    PC.printf("Flag_SpeedZone: %d\r\n\n", flag_SpeedZone);
    
    if(1 == flag_SpeedZone)
    {
        PC.printf("\t\t\t\t Max_Linii: %d\r\n", max_linii);
        if(max_linii >= 4) 
        {
            flag_MotorLow = 1;
            flag_MotorHigh = 0;    
        }   
        else
        {
            flag_MotorLow = 0;
            flag_MotorHigh = 1;    
                
        }
    }

    if(flag_MotorLow == 0 && flag_MotorHigh == 1)
    {
        acc_left = 0.15;
        acc_right = 0.15;
        rled = 1;
        bled = 0;    
    }
    else if(flag_MotorLow == 1 && flag_MotorHigh == 0)
    {
        acc_left = 0.12;
        acc_right = 0.12;
        rled = 0;
        bled = 1;    
    }           
            
}


void calculate_motors_speed()
{
    float acc_maximum=0;
    /* get the maximum speed of the two wheels */
             if (steer>0)
            {
             acc_maximum = acc_right;
             }
            else{
                acc_maximum = acc_left;
             }

             if(acc_maximum < acc_min)
             {
                 acc_maximum = acc_min;
             }
             /******************************************/
             
             if ((steer >= -0.09 && steer <= 0.09))
            {
                if(time_to_acc > 1){
                    time_to_acc -= 1;
                    acc_left = acc_maximum + (acc_max - acc_maximum)/time_to_acc;
                    acc_right = acc_left; 
                }
                else{
                    acc_left = acc_max ;
                    acc_right = acc_max ;    
                }
            }
            else if (steer >0 ){
             time_to_acc = 10;
             acc_left = acc_maximum * (1 - 7*steer/10);
             acc_right = acc_maximum * (1- 2*steer/10);  
            }
            else{
             time_to_acc = 10;
             acc_right = acc_maximum * (1+ 7*steer/10);   
             acc_left = acc_maximum * (1 + 2*steer/10);  
            }
}

void update_servo_command()
{
    TFC_SetServo(0, -steer);
}

void stop()
{
    TFC_SetMotorPWM(0.2, 0.2);
    wait(0.5);
    TFC_SetMotorPWM(0, 0);
    wait(1000);
}
