
#ifndef tsm_ac_h
#define tsm_ac_h

#include "TSISensor.h"

/*  defines area    */
// change this define for the car you are working with
// car0 = the Bucharest champion
// car1 = the other champion
#define car0
/*********************/

extern TSISensor tsi_button;


extern DigitalOut rled;
extern DigitalOut gled;
extern DigitalOut bled;

extern Serial PC;
extern RawSerial Xbee;


#endif