from Tkinter import Tk, Canvas, mainloop
import random


class CameraDisplay:
    def __init__(self):
        self.no_of_pixel = 128
        self.pixel_size = 10
        self.pixel_h = 50
        self.max_pixel_value = 255
        self.max_color_level = 99
        self.color_name = "gray"

        self.WIDTH, self.HEIGHT = self.pixel_size*128, self.pixel_h
        self.window = Tk()
        self.canvas = Canvas(self.window, width=self.WIDTH, height=self.HEIGHT, bg="#ffffff")
        for i in range(0, self.no_of_pixel - 1):
            self.canvas.create_rectangle(self.pixel_size*i, 0, self.pixel_size*(i+1), self.pixel_h, fill="white", outline="white", tag=str(i))
        self.canvas.pack()

    def get_gray_color_name(self, byte_value):
        color_string = 0
        if byte_value <= self.max_pixel_value:
            color_mapping = self.max_color_level * byte_value / self.max_pixel_value
            color_string = self.color_name + str(color_mapping)
        else:
            print "Maximum value accepted for Pixel Color is " + str(self.max_pixel_value)
        return color_string

    def update_interface(self, list_of_data):
        if len(list_of_data) == self.no_of_pixel:
            for index in range(0, self.no_of_pixel - 1):
                color = self.get_gray_color_name(list_of_data[index])
                self.canvas.itemconfig(str(index), fill=color, outline=color)
        else:
            print "List should have " + str(self.no_of_pixel) + " values"


def generate_new_set_of_value():
    generated_data = random.sample(range(0, 255), 128)
    return generated_data


if __name__ == "__main__":
    cam = CameraDisplay()
    data = generate_new_set_of_value()
    print data
    cam.update_interface(data)
    mainloop()