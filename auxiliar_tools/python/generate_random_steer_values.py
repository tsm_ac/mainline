
import random
import os

if os.path.exists("steer_values"):
    os.remove("steer_values")
f = open("steer_values", "w")

for i in range(1,1000):
    value = str(random.uniform(-34, 36))
    f.write(value + "\r\n")

f.close()
