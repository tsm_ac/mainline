import matplotlib.pyplot as plt
import random
import serial
plt.ion()
x = range(0, 128)

# serial port
sp = "/dev/ttyACM0"
# connect to a serial port
ser = serial.Serial(sp, 115200, timeout=1)

ax = plt.gca()
flag = 0
plt.title("Camera stream")
camera_calib = ""


while True:
    camera_pixels = ser.read(2000).decode()
    ind = 0
    start = -1
    for pixel in range(0, len(camera_pixels)):
        if camera_pixels[pixel] == '<':
            start = ind
            break
        ind = ind + 1
    for pixel in range(start+1, len(camera_pixels)):
        if camera_pixels[pixel] == '>':
            end = ind
            break
        ind = ind + 1

    #print(camera_pixels)
    if start != -1 and end != -1 and start < end:
        camera_pixels = camera_pixels[start+1:end+1]
        camera_pixels = camera_pixels.split(',')
        print(camera_pixels)

        y = []
        for pixel in camera_pixels:
            y.append(float(pixel))
        print(y)


        # plt.gca().cla() # optionally clear axes
        plt.clf()
        plt.ylim(top=1000)  # adjust the top leaving bottom unchanged
        plt.ylim(bottom=0)  # adjust the bottom leaving top unchanged
        plt.plot(x, y)

        plt.draw()
        plt.pause(0.1)


